import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'RestaurantsListModel.dart';

class GoogleMaps extends StatefulWidget {

  @override
  _GoogleMapsState createState() => _GoogleMapsState();
}

class _GoogleMapsState extends State<GoogleMaps> {

  GoogleMapController mapController;
  List<Marker> _markers =[];
  LatLng _center = const LatLng(31.484527,35.029474);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }



  void getMarkers(){
    _markers.clear();
    for (final res in Provider.of<RestaurantsListModel>(context, listen: false).getAll()) {
      final marker = Marker(
          markerId: MarkerId(res.name),
          position: LatLng(res.latitude, res.longitude),
          infoWindow: InfoWindow(
            title: res.name,
            snippet: "Rating: "+res.rating.toString()+"/10",
          )
      );
      _markers.add(marker);
    }

  }

  @override
  void initState() {
    super.initState();
    getMarkers();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("Restaurants on Map",
            style: TextStyle(
                color: Colors.black
            )
        ),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
        ),
        body: GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 11.0,
          ),
          markers: _markers.toSet(),
        ),
      ),
    );
  }
}
