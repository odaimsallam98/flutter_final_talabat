import 'package:flutter/material.dart';
import 'package:flutter_app_talabat/FavouritesListModel.dart';
import 'package:flutter_app_talabat/RestaurantsListModel.dart';
import 'DataBase.dart';
import 'LoadingRes.dart';
import 'package:provider/provider.dart';
import 'OrdersListModel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context){
    return ChangeNotifierProvider(
        create: (context) => OrdersListModel(),
        child: ChangeNotifierProvider(
          create: (context) => RestaurantsListModel(),
          child: ChangeNotifierProvider(
              create: (context) => FavouritesListModel(),
              child: ChangeNotifierProvider(
                  create: (context)=> DatabaseProvider.db,
                  child: MaterialApp(
                    title: 'Flutter Demo',
                    initialRoute: '/',
                    routes: {
                      '/': (context) => Loading(),
                    },
                  )
              )
          )
        )
    );
  }
}



