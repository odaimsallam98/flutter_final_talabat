import 'package:flutter/material.dart';
import 'package:flutter_app_talabat/FavouritesListModel.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'DataBase.dart';
import 'MenuItem.dart';
import 'OrdersList.dart';
import 'OrdersListModel.dart';

int cardHeight;

class MenuItemsList extends StatefulWidget {
  final List<MenuItem> menuItemList;
  final String name;
  MenuItemsList({this.menuItemList,this.name});
  @override
  _MenuItemsListState createState() => _MenuItemsListState(this.menuItemList,this.name);
}

class _MenuItemsListState extends State<MenuItemsList> {
  List<MenuItem> menuItemList;
  String name;
  _MenuItemsListState(this.menuItemList,this.name);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(name + " Menu ",
            style: TextStyle(
                color: Colors.black
            )),
            actions: [
              IconButton(
                icon: Icon(Icons.playlist_add_check),
                onPressed: () {
                 Navigator.push(
                   context,
                   MaterialPageRoute(builder: (context) => OrdersList()),
                 );
                },
              ),
            ],
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
        ),
        body:
        CarouselSlider(
          options:
          CarouselOptions(
            height: (0.8*(MediaQuery. of(context). size. height)),
            aspectRatio: 16/9,
            viewportFraction: 0.8,
            initialPage: 0,
            enableInfiniteScroll: true,
            reverse: false,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 3),
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            enlargeCenterPage: true,
            // onPageChanged: callbackFunction,
            scrollDirection: Axis.horizontal,
          ),
          items: this.menuItemList.map((i) {
            return Builder(
              builder: (BuildContext context) {

                return MenuItemWidget(i,1);
              },
            );
          }).toList(),
        )
    );
  }
}

class MenuItemWidget extends StatefulWidget {
  final MenuItem _menu_item;
  final int index;
  MenuItemWidget(this._menu_item,this.index);
  @override
  _MenuItemWidgetState createState() => _MenuItemWidgetState(this._menu_item,this.index);
}

class _MenuItemWidgetState extends State<MenuItemWidget> {
  final MenuItem _menu_item;
  final int index;
  _MenuItemWidgetState(this._menu_item,this.index);
 // bool addedToFavouritesList = false;

  @override
  void initState() {
    super.initState();
   //checkFavourite();
  }

  @override
  Widget build(BuildContext context) {
/*
    Future<void> checkFavourite()async{
      bool result = await Provider.of<DatabaseProvider>(context, listen: false).favouriteExists(this._menu_item.id,this._menu_item.rest_id);
      this.addedToFavouritesList = result;
    }

 */
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.white, Colors.blue])),

      margin: new EdgeInsets.symmetric(vertical: 40),
      child: Column(

        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Card(
              child:Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children:[
                    Container(
                        height: 200,
                        child: Image.network("http://appback.ppu.edu/static/"+this._menu_item.image)
                    ),
                    Text(
                      this._menu_item.name, style: TextStyle(fontWeight: FontWeight.bold),),
                    Text(this._menu_item.description),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children:[
                        Consumer<DatabaseProvider>
                          (builder: (context, databaseProvider, child) {
                            return FutureBuilder<bool>(
                                future:Provider.of<DatabaseProvider>(context,listen: true).favouriteExists(this._menu_item.id, this._menu_item.rest_id),
                                builder: (context,snapshot){
                                  if(snapshot.hasData){
                                    bool addedToFavouritesList = snapshot.data;
                                    return  IconButton(
                                        icon: Icon(
                                          addedToFavouritesList? Icons.favorite_outlined :Icons.favorite_outline,
                                        ),
                                        onPressed: () {
                                          addedToFavouritesList?
                                          Provider.of<DatabaseProvider>(context, listen: false).deleteFavourite(this._menu_item.id,this._menu_item.rest_id):
                                          Provider.of<DatabaseProvider>(context, listen: false).addFavourite(this._menu_item);
                                        }
                                    );
                                  }
                                  else if(snapshot.hasError){
                                    return Text("error ${snapshot.error}");
                                  }
                                  return Center(
                                    child: SpinKitPouringHourglass(
                                      color: Colors.black,
                                      size: 50.0,
                                    ),
                                  );
                                }
                            );
                        } ),


                        Consumer<OrdersListModel>
                          (builder: (context, ordersList, child){
                          bool addedToOrdersList = ordersList.itemExists(_menu_item);
                          return  IconButton(
                              icon: Icon(
                                addedToOrdersList? Icons.delete :Icons.add,
                              ),
                              onPressed: () {
                                ordersList.itemExists(_menu_item)?
                                Provider.of<OrdersListModel>(context, listen: false).remove(_menu_item):
                                Provider.of<OrdersListModel>(context, listen: false).add(this._menu_item);
                              }
                          );
                        } ),
                      ],
                    ),
                    Text('Price:'+this._menu_item.price.toString())
                  ])
          )
        ],

      ),
    );
  }
}

/*
Consumer<FavouritesListModel>
(builder: (context, favouritesList, child){
bool addedToFavouritesList = favouritesList.itemExists(_menu_item);
return  IconButton(
icon: Icon(
addedToFavouritesList? Icons.favorite_outlined :Icons.favorite_outline,
),
onPressed: () {
favouritesList.itemExists(_menu_item)?
Provider.of<FavouritesListModel>(context, listen: false).remove(_menu_item):
Provider.of<FavouritesListModel>(context, listen: false).add(this._menu_item);
}
);
} ),


 */

/*

 */


