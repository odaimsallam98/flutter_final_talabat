import 'dart:ui';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'Restaurant.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'MenuItemsList.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class RestaurantsList extends StatefulWidget {
  final List<Restaurant> restaurants;
  RestaurantsList({this.restaurants});

  @override
  _RestaurantsListState createState() => _RestaurantsListState(restaurants);
}

class _RestaurantsListState extends State<RestaurantsList> {

  List<Restaurant> restaurants;

  _RestaurantsListState(this.restaurants);

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(title: Text("Restaurants List",
          style: TextStyle(
              color: Colors.black
          )
      ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),

      body: CarouselSlider(
        options:
        CarouselOptions(
          height: ((MediaQuery. of(context). size. height)),

          viewportFraction: 0.8,
          initialPage: 0,
          enableInfiniteScroll: false,
          reverse: false,

          autoPlayInterval: Duration(seconds: 3),
          autoPlayAnimationDuration: Duration(milliseconds: 800),
          autoPlayCurve: Curves.fastOutSlowIn,


          aspectRatio: 2.0,
          enlargeCenterPage: true,
          scrollDirection: Axis.vertical,
          autoPlay: true,
          // onPageChanged: callbackFunction,


        ),
        items: restaurants.map((i) {
          return Builder(
            builder: (BuildContext context) {
              return RestaurantItem(i,1);
            },
          );
        }).toList(),
      ),
    );
  }
}

class RestaurantItem extends StatefulWidget {
  final Restaurant _restaurant;
  final int index;

  RestaurantItem(this._restaurant,this.index);
  @override
  _RestaurantItemState createState() => _RestaurantItemState(this._restaurant,this.index);
}

class _RestaurantItemState extends State<RestaurantItem> {
  final Restaurant _restaurant;
  final int index;
  _RestaurantItemState(this._restaurant,this.index);

  double rank=0;

  void setRankForRes()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      print("from set state in stful widget");
      this.rank = prefs.getDouble('${this._restaurant.id.toString()}')?? 0;

    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MenuItemsList(menuItemList:_restaurant.menu_items,name:_restaurant.name)),
          );
        },
        child:Card(
            child:Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.white, Colors.black])),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children:[
                    Container(
                        height: 200,
                        width:  0.8*((MediaQuery. of(context). size.width)),
                        child: Image.network("http://appback.ppu.edu/static/"+this._restaurant.image_name)
                    ),
                    Text(
                      this._restaurant.name, style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),),
                    Text(this._restaurant.city,style: TextStyle(color: Colors.white),),
                    ElevatedButton.icon(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (_) =>
                              RateAlertDialog(
                                  name: this._restaurant.name,
                                  id: this._restaurant.id,
                                  setRank:(){setRankForRes();}),
                          barrierDismissible: false,
                        );
                        },

                      style: ElevatedButton.styleFrom(
                        primary: Colors.teal,
                      ),
                      icon: rank>0?Icon(Icons.star_rate, size: 18):Icon(Icons.star_rate_outlined, size: 18),
                      label: rank>0?Text("RATED"):Text("RATE"),
                    ),


                    RatingBar.builder(
                      initialRating: this._restaurant.rating/2,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                    )

                  ]),
            )

        )
    );
  }
}


class RateAlertDialog extends StatelessWidget {
  final String name;
  final int id;
  final VoidCallback setRank;

  void _rateRestaurant(double rate) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble('${this.id.toString()}', rate);
  }


  RateAlertDialog({this.name,this.id,this.setRank});
  @override
  Widget build(BuildContext context) {
    double rate=5.0;
    return AlertDialog(
      title: Text('Rate '+this.name),
      content: RatingBar.builder(
        initialRating: 3,
        minRating: 1,
        direction: Axis.horizontal,
        allowHalfRating: true,
        itemCount: 5,
        itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
        itemBuilder: (context, _) => Icon(
          Icons.star,
          color: Colors.amber,
        ),
        onRatingUpdate: (rating) {
          //print(rating);
          rate = rating*2;
        },
      ),
      actions: <Widget>[
        new FlatButton(
          child: new Text('Rate'),
          onPressed: () {
            this._rateRestaurant(rate);
            setRank();
            Navigator.of(context).pop();
          },
        )
      ],
    ) ;
  }
}