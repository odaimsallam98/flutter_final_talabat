import 'package:flutter/cupertino.dart';
import 'package:flutter_app_talabat/Restaurant.dart';

class RestaurantsListModel extends ChangeNotifier{
   List<Restaurant> _restaurantsList = [];

  int get numItems => _restaurantsList.length;

  Restaurant getRestaurant(int index) => _restaurantsList[index];

  bool itemExists(Restaurant R) => _restaurantsList.indexOf(R)>=0 ? true: false;

  void add(Restaurant R){
    _restaurantsList.add(R);
    notifyListeners();
  }
  List<Restaurant> getAll(){
    return _restaurantsList;
    notifyListeners();
  }

  void remove(Restaurant R){
    _restaurantsList.remove(R);
    notifyListeners();
  }
  void setAll(List<Restaurant> RL){
    this._restaurantsList = RL;
    notifyListeners();
  }
  void removeAll(){
    _restaurantsList.clear();
    notifyListeners();
  }
}