import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'DataBase.dart';
import 'DataBase.dart';
import 'FavouritesListModel.dart';
import 'MenuItem.dart';
import 'MenuItemsList.dart';
import 'package:provider/provider.dart';

class FavouritesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Favourites List",
            style: TextStyle(
                color: Colors.black
            )
        ),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
        ),
        body: Consumer<DatabaseProvider>(
          builder: (context, databaseProvider, child){
            return FutureBuilder<List<MenuItem>>(
                future:databaseProvider.getAllFavourites(),
                builder: (context,snapshot){
                  if(snapshot.hasData){
                    List<MenuItem> favouritesList = snapshot.data;
                    //Provider.of<RestaurantsListModel>(context,listen: true).setAll(res);
                    return  ListView.builder(
                      itemCount:  favouritesList.length,
                      itemBuilder: (BuildContext context, int index){
                        return MenuItemWidget(favouritesList[index],index);
                      },
                    );
                  }
                  else if(snapshot.hasError){
                    return Text("error ${snapshot.error}");
                  }
                  return Center(
                    child: SpinKitPouringHourglass(
                      color: Colors.black,
                      size: 50.0,
                    ),
                  );
                }
            );
          },
        )
    );
  }
}

//DatabaseProvider.db.getAllFavourites()

