import 'package:flutter/cupertino.dart';
import 'MenuItem.dart';

class FavouritesListModel extends ChangeNotifier{
  final List<MenuItem> _favouritesList = [];

  int get numItems => _favouritesList.length;

  MenuItem getItem(int index) => _favouritesList[index];

  bool itemExists(MenuItem MI) => _favouritesList.indexOf(MI)>=0 ? true: false;

  void add(MenuItem menuItem){
    _favouritesList.add(menuItem);
    notifyListeners();
  }

  void remove(MenuItem MI){
    _favouritesList.remove(MI);
    notifyListeners();
  }

  void removeAll(){
    _favouritesList.clear();
    notifyListeners();
  }
}