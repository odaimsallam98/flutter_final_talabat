import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_app_talabat/Home.dart';
import 'package:flutter_app_talabat/RestaurantsListModel.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'Restaurant.dart';
import 'MenuItem.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}
class _LoadingState extends State<Loading> {
  Future<List<Restaurant>> restaurants;
  Future<List<MenuItem>> fetchMenuItems(int id, String name) async {

    final http.Response response = await http.get('http://appback.ppu.edu//menus/'+id.toString());

    if(response.statusCode == 200) {
      List jsonArray = jsonDecode(response.body);
      List<MenuItem> menuItems = jsonArray.map((e) => MenuItem.fromJson(e)).toList();
      return menuItems;
    }
    else{
      throw Exception("failed to load MenuItems data of "+name+" restaurant");
    }
  }


  Future<List<Restaurant>> fetchRestaurants() async {
    final http.Response response = await http.get('http://appback.ppu.edu/restaurants');
    if(response.statusCode == 200) {
      List jsonArray = jsonDecode(response.body);
      List<Restaurant> restaurants = jsonArray.map((e) => Restaurant.fromJson(e)).toList();
      for(int i=0;i<restaurants.length;i++)
        {
          restaurants[i].menu_items= await fetchMenuItems(restaurants[i].id,restaurants[i].name);
        }
      return restaurants;
    }
    else{
      throw Exception("failed to load data");
    }
  }

  @override
  void initState() {
    super.initState();
    this.restaurants = fetchRestaurants();
  }

  @override
  Widget build(BuildContext context){
    return FutureBuilder<List<Restaurant>>(
        future:this.restaurants,
        builder: (context,snapshot){
          if(snapshot.hasData){
           List<Restaurant> res = snapshot.data;
           Provider.of<RestaurantsListModel>(context,listen: true).setAll(res);
           return  Home();
          }
          else if(snapshot.hasError){
            return Text("error ${snapshot.error}");
          }
          return Center(
            child: SpinKitPouringHourglass(
              color: Colors.black,
              size: 50.0,
            ),
          );
        }
    );
  }

}
/*
Consumer<RestaurantsListModel>
             (builder: (context, restaurantsList, child){
             Provider.of<RestaurantsListModel>(context, listen: false).setAll(res);

           } );
 */