import 'package:flutter/cupertino.dart';
import 'MenuItem.dart';

class OrdersListModel extends ChangeNotifier{
  final List<MenuItem> _ordersList = [];

  int get numItems => _ordersList.length;

  MenuItem getItem(int index) => _ordersList[index];
  double getTotalPrice(){
    double total=0;
    for(int i =0; i <this._ordersList.length;i++){
      total += this._ordersList[i].price;
    }
    return total;
  }
  bool itemExists(MenuItem MI) => _ordersList.indexOf(MI)>=0 ? true: false;

  void add(MenuItem menuItem){
    _ordersList.add(menuItem);
    notifyListeners();
  }

  void remove(MenuItem MI){
    _ordersList.remove(MI);
    notifyListeners();
  }

  void removeAll(){
    _ordersList.clear();
    notifyListeners();
  }
}