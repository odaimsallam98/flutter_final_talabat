
class MenuItem{
  final int id;
  final int rest_id;
  final String name;
  final String description;
  final double price;
  final String image;
 // final double rating;

  static final columns = ['id','rest_id', 'name', 'description', 'price','image'];

  MenuItem({this.id,this.rest_id,this.name,this.description,this.image,this.price,
   // this.rating
  });

  factory MenuItem.fromJson(dynamic jsonObject){
    return MenuItem(
        id: jsonObject['id'],
        rest_id: jsonObject['rest_id'],
        name: jsonObject['name'],
        description: jsonObject['descr'],
        image: jsonObject['image'],
        price: double.parse(jsonObject['price'].toString()),
       // rating: double.parse(jsonObject['rating'].toString()),
    );
  }

  factory MenuItem.fromMap(Map<String, dynamic> data){
    return MenuItem(
        id: data['id'],
        rest_id: data['rest_id'],
        name: data['name'],
        description: data['description'],
        price: data['price'],
        image: data['image'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "rest_id":rest_id,
      "name": name,
      "description": description,
      "price": price,
      "image":image
    };
  }








}