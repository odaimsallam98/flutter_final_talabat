import 'package:flutter/material.dart';
import 'MenuItemsList.dart';
import 'package:provider/provider.dart';

import 'OrdersListModel.dart';
class OrdersList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Orders List",
            style: TextStyle(
            color: Colors.black
        )
        ),
      backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
    ),
        body:
          Consumer<OrdersListModel>(
            builder: (context, ordersList, child){
              return ListView.builder(
                itemCount: ordersList.numItems,
                itemBuilder: (BuildContext context, int index){
                return MenuItemWidget(ordersList.getItem(index),index);
                },
              );
            },
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          floatingActionButton: FloatingActionButton(
        // isExtended: true,
               child: Text( "" + Provider.of<OrdersListModel>(context, listen: false).getTotalPrice().toString(),
            style: TextStyle(
                color: Colors.black
            )),
            backgroundColor: Colors.green,
            onPressed: () {
        },
      ),
    );
  }
}
