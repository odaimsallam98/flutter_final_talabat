import 'package:flutter/cupertino.dart';
import 'package:flutter_app_talabat/MenuItem.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider extends ChangeNotifier {

  DatabaseProvider._();
  static final DatabaseProvider db = DatabaseProvider._();
  static Database _database;
  static final int version = 1;

  Future<Database> get database async{
    if (_database != null)
      return _database;

    _database = await initDB();
    return _database;
  }

  initDB() async {
    String path = join(await getDatabasesPath(), 'uniDB.db');
    return await openDatabase(
        path,
        version: version,
        onCreate: (Database db, int version) async {
          await db.execute(
              '''             
              CREATE TABLE favourites (
              id INTEGER ,
              rest_id INTEGER ,
              name TEXT,
              description TEXT,
              price REAL,
              image TEXT)  
              '''
          );
        }
    );
  }

  Future<List<MenuItem>> getAllFavourites() async {
    final db = await database;
    List<Map> results = await db.query(
        'favourites', columns: MenuItem.columns, orderBy: "id ASC"
    );
    List<MenuItem> favourites = List();
    results.forEach((result) {
      MenuItem favourite = MenuItem.fromMap(result);
      favourites.add(favourite);
    });
    return favourites;
  }


  Future<List<MenuItem>> getAllOrders() async {
    final db = await database;
    List<Map> results = await db.query(
        'orders', columns: MenuItem.columns, orderBy: "id ASC"
    );
    List<MenuItem> orders = List();
    results.forEach((result) {
      MenuItem order = MenuItem.fromMap(result);
      orders.add(order);
    });
    return orders;
  }

  insert(MenuItem menuItem) async {
    final db = await database;
    var result = await db.rawInsert(
        '''
      INSERT INTO favourites ( id , rest_id , name , description , image , price) VALUES (?, ?, ?, ?, ?, ?)
      ''',
        [menuItem.id,menuItem.rest_id,menuItem.name, menuItem.description, menuItem.image,menuItem.price]
    );
    print("inserted from insert");

    return result;
  }
  addFavourite(MenuItem favourite) async {
    final db = await database;
    db.insert('favourites', favourite.toMap());
    print("inserted from insert");
    notifyListeners();
  }

  addOrder(MenuItem order) async {

    final db = await database;
    db.insert('student', order.toMap());
    notifyListeners();
  }

  deleteFavourite(int id, int rest_id) async {
    final db = await database;
    db.delete('favourites', where: 'id=? AND rest_id=?', whereArgs: [id,rest_id]);
    print("delete the favourite");
    notifyListeners();
  }
  deleteOrder(int id, int rest_id) async {
    final db = await database;
    db.delete('orders', where: 'id=? AND rest_id=?', whereArgs: [id,rest_id]);
    notifyListeners();
  }

  Future<bool> favouriteExists(int id, int rest_id) async {
    bool result;
    final db = await database;
    var queryResult = await db.rawQuery('SELECT * FROM favourites WHERE id=? AND rest_id=?',[id,rest_id]);
    queryResult.isNotEmpty ? result=true : result=false;
    print("detect the item exist or not");
    return result;
  }


}




/*
  deleteAll() async {
    final db = await database;
    db.delete('student');
    notifyListeners();
  }


  deleteStudent(int id) async {
    final db = await database;
    db.delete('student', where: 'id=?', whereArgs: [id]);
    notifyListeners();
  }


               CREATE TABLE student (
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              name TEXT,
              major TEXT,
              avg REAL)




*/


/*

              CREATE TABLE orders (
              id INTEGER PRIMARY KEY,
              rest_id INTEGER PRIMARY KEY,
              name TEXT,
              description TEXT,
              price REAL,
              image TEXT)

 */